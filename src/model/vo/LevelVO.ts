export enum LevelState {
  COMPLETED,
  UNLOCKED,
  LOCKED,
}

export default class LevelVO {
  public state: LevelState;
  public index: number;
  public starsCount: number;

  constructor(index: number) {
    this.state = LevelState.LOCKED;
    this.index = index;
    this.starsCount = 0;
  }
}
