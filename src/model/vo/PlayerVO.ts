import LevelVO, { LevelState } from './LevelVO';
export enum setting {
  MUSIC,
  SOUND,
  ADS,
  LANGUAGE,
}

export default class PlayerVO {
  public id: number;
  public name: string;
  public maxLevel: number;
  public levels: LevelVO[];
  public playerHasChance: boolean;
  public settings: ISettings;
  public addonHints: number;
  public tutorialComplete: boolean;
  public score: number;
  public maxScore: number;

  constructor(name: string, id: number) {
    this.id = id;
    this.name = name;
    const firstLevel: LevelVO = new LevelVO(1);
    firstLevel.state = LevelState.UNLOCKED;
    this.maxLevel = 1;
    this.levels = [firstLevel];
    const lang: string = window.navigator.language;
    const langCode: string = lang.substr(0, lang.indexOf('-'));
    this.settings = {
      music: true,
      sound: true,
      ad: true,
      lang: langCode,
    };
    this.tutorialComplete = false;
    this.score = 0;
    this.maxScore = 0;
  }
}

export interface ISettings {
  music: boolean;
  sound: boolean;
  ad: boolean;
  lang: string;
}
