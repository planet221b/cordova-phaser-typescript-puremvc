import { SimpleCommand, SyncMacroCommand } from '@koreez/pure-mvc';
import ChancePopup from '../view/popups/ChancePopup';
import ShowChanceVideoCommand from './ad/ShowChanceVideoCommand';

export default class RegisterAdsCommands extends SyncMacroCommand<
  SimpleCommand
> {
  public execute(): void {
    this.facade.registerCommand(
      ChancePopup.WATCH_CLICKED_NOTIFICATION,
      ShowChanceVideoCommand,
    );
  }
}
