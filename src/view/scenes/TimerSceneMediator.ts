import BaseSceneMediator from './BaseSceneMediator';
import TimerScene from './TimerScene';

export default class TimerSceneMediator extends BaseSceneMediator<TimerScene> {
  public static NAME: string = 'TimerSceneMediator';

  constructor() {
    super(TimerSceneMediator.NAME, null);
  }

  public listNotificationInterests(): string[] {
    return [
    ];
  }

  public handleNotification(notificationName: string): void {
    switch (notificationName) {
      case '':
        this.scene.start(TimerScene.NAME);
        this.scene.bringToTop(TimerScene.NAME);
        this.viewComponent.startTimer();
        break;
      default:
        console.warn(`${notificationName} is unhandled!`);
        break;
    }
  }
  protected setView(): void {
    const timerScene: TimerScene = new TimerScene();
    this.scene.add(TimerScene.NAME, timerScene);
    this.setViewComponent(timerScene);
    this.setViewListeners();
    super.setView();
  }

  private setViewListeners(): void {
    this.viewComponent.events.on('timerComplete', this.onTimerComplete, this);
  }

  private onTimerComplete(): void {
    this.sendNotification(TimerScene.TIMER_COMPLETE);
    this.scene.stop(TimerScene.NAME);
  }
}
