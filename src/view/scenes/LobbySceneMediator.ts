import BaseSceneMediator from './BaseSceneMediator';
import LobbyScene from './LobbyScene';
import PreloadScene from './PreloadScene';

export default class LobbySceneMediator extends BaseSceneMediator<LobbyScene> {
  public static NAME: string = 'LobbySceneMediator';

  constructor() {
    super(LobbySceneMediator.NAME, null);
  }

  public listNotificationInterests(): string[] {
    return [PreloadScene.LOAD_COMPLETE_NOTIFICATION];
  }

  public handleNotification(notificationName: string): void {
    switch (notificationName) {
      case PreloadScene.LOAD_COMPLETE_NOTIFICATION:
        this.scene.start(LobbyScene.NAME);
        break;
      default:
        console.warn(`${notificationName} is unhandled!`);
        break;
    }
  }

  protected setView(): void {
    const lobbyScene: LobbyScene = new LobbyScene();
    this.scene.add(LobbyScene.NAME, lobbyScene);
    this.setViewComponent(lobbyScene);
    super.setView();
  }
}
