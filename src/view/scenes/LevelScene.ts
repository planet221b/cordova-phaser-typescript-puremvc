import BaseScene from './BaseScene';

export default class LevelScene extends BaseScene {
  public static NAME: string = 'LevelScene';

  constructor() {
    super(LevelScene.NAME);
  }
}
