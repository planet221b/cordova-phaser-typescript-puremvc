import BaseSceneMediator from './BaseSceneMediator';
import LevelScene from './LevelScene';

export default class LevelSceneMediator extends BaseSceneMediator<LevelScene> {
  public static NAME: string = 'LevelSceneMediator';

  constructor() {
    super(LevelSceneMediator.NAME, null);
  }

  public listNotificationInterests(): string[] {
    return [];
  }

  public handleNotification(notificationName: string): void {
    switch (notificationName) {
      default:
        console.warn(`${notificationName} is unhandled!`);
        break;
    }
  }

  protected setView(): void {
    const levelScene: LevelScene = new LevelScene();
    this.scene.add(LevelScene.NAME, levelScene);
    this.setViewComponent(levelScene);
    super.setView();
  }
}
