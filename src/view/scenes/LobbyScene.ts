import BaseScene from './BaseScene';

export default class LobbyScene extends BaseScene {
  public static NAME: string = 'LobbyScene';

  constructor() {
    super(LobbyScene.NAME);
  }
}
