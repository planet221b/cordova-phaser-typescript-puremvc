import { gameConfig } from '../../constants/GameConfig';
import { loopRunnable, removeRunnable } from '../../utils/Utils';
import BaseScene from './BaseScene';

export default class TimerScene extends BaseScene {
  public static NAME: string = 'TimerScene';
  public static TIMER_COMPLETE: string = `${TimerScene.NAME}TimerComplete`;
  private timer: Phaser.GameObjects.Text;
  private timerRunnable: Phaser.Time.TimerEvent;
  private timerValue: number;

  constructor() {
    super(TimerScene.NAME);
  }

  public create(): void {
    this.timerValue = 3;
    const style: any = {
      fontFamily: 'Arial',
      fontSize: 40,
    };
    this.timer = this.add.text(
      gameConfig.canvasWidth / 2,
      gameConfig.canvasHeight / 2,
      '3',
      style,
    );
    this.timer.setOrigin(0.5);
    this.timer.setVisible(true);
  }

  public startTimer(): void {
    this.timer.setVisible(true);
    this.timer.setScale(2);
    this.timer.setText(`${this.timerValue}`);
    this.tweens.add({
      targets: this.timer,
      duration: 200,
      scaleX: 1,
      scaleY: 1,
    });
    this.timerRunnable = loopRunnable(this, 1000, this.tickTimer, this);
  }

  private tickTimer(): void {
    this.timerValue--;
    if (this.timerValue < 0) {
      removeRunnable(this.timerRunnable);
      this.timerRunnable = null;
      this.events.emit('timerComplete');
      return;
    }
    this.timer.setScale(2);
    this.timer.setText(`${this.timerValue}`);
    this.tweens.add({
      targets: this.timer,
      duration: 200,
      scaleX: 1,
      scaleY: 1,
    });
  }
}
